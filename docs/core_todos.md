Overview of topics that should or could be worked on in the immediate future (a.k.a. upcoming months and next year). Here they are split in three main categories:
* Technical/low-level improvements.
* 5.0 compatibility breakages.
* More user-facing projects.

Topics that are likely to be the primary focus of upcoming months are tagged with **[2024 Q4]**.

## Maintenance & Technical Debt

General target of this section is to improve quality, reliability and maintainability of existing Blender code and features. Ideally it should also make adding new features simpler and safer.

### Testing **[2024 Q4]**

Add more periodic builds to the buildbot. The main goal is to detect early issues that can easily skip through our unittests.
* Add [code-coverage reports](https://projects.blender.org/infrastructure/blender-projects-platform/issues/117).
* [Run unittests on debug builds](https://projects.blender.org/infrastructure/blender-projects-platform/issues/127). Ran nightly, using debug-with-ASAN builds.
* Add 'smoke tests' (opening and rendering a frame from a collection of complex production files from the past 2 or 3 years). Ran weekly probably, e.g. on the weekends, using release-with-assert builds.

### RNA Path Project **[2024 Q4]**

Moving PointerRNA to non-trivial C++ data has proven complex, in particular because of its links with python integration code. Current code is "95%" there, but unclear how hard it will be to tackle the last percents.

Related design tasks:
* [RNA: Make PointerRNA aware of its own 'data path'](https://projects.blender.org/blender/blender/issues/122431)
* [How to deal with non-trivial C++ data in BPY](https://projects.blender.org/blender/blender/issues/124794)

### Finish "C++ GuardedAlloc" Sanitizing

Lots of work done there already, but we still have some half-baked features left over, the need to be either finished, or fully removed. See also the [WIP design task](https://projects.blender.org/blender/blender/issues/124512).

### Handling 'Unused IDs' on Save & Fake User

This is an old known TODO to finalize the design for this rework of how unused data is handled when writing a blendfile.
* [Always write unused IDs on save](https://projects.blender.org/blender/blender/issues/61209)
* [Recursive Purge Orphans datablocks on file save](https://projects.blender.org/blender/blender/issues/87490)

### Other Random Topics

There are a lot of small or not-so-small designs/todos in [the Core workboard](https://projects.blender.org/blender/blender/projects/3). Some things that would be 'good to have soon':
* [RNA API: Support Proper Deprecation Process](https://projects.blender.org/blender/blender/issues/127876).
* [RNA API: Add Optional API/Dev documentation](https://projects.blender.org/blender/blender/issues/128531).
* ID Management: Sanitize and clarify [ID tags](https://projects.blender.org/blender/blender/issues/88555) and [ID creation/copy tags](https://projects.blender.org/blender/blender/issues/90610) _(was already part of last 'technical debt' mini-project schedule)_.
* [BKE: Refactor handling of packed files to use IDTypeInfo](https://projects.blender.org/blender/blender/issues/122148).
* [Refactor Delete Hierarchy Outliner code](https://projects.blender.org/blender/blender/issues/119127)
* [WIP: Improve Copy/Paste](https://projects.blender.org/blender/blender/issues/116890) _(although this one is arguably more of an 'other modules' topic ultimately)_.

## 5.0 Breaking Changes **[2024 Q4]**

Up-coming Blender 5.0 next year is the opportunity to introduce forward-compatibility breaking changes. Since Blender 4.5 is expected to be able to open 5.x blendfiles, it means that designs and targets should be discussed and agreed on in the next few months, and implementation should happen in the first half of 2025.

In general, there is need for coordination for this topic (an overview of ideas can be found in tasks listed in the [5.0 milestone](https://projects.blender.org/blender/blender/milestone/20)).

### Blendfile

_Not all ideas listed here would necessarily imply compatibility breakage._

* Enhanced blendfile header/meta-informations (data-block index, bigger thumbnail...).
* Support shared data accross IDs.
* Improve binary stability of blendfiles across saving when no actual data is changed.

See also [the dedicated task](https://projects.blender.org/blender/blender/issues/90912).

### IDProperties

[Separate storage of IDproperties](https://projects.blender.org/blender/blender/issues/123232) defined by users as 'custom data', from the ones used by runtime RNA as storage backend (e.g. all data defined by extensions).

## (New) Projects

Ideas and designs to either add new features or improve existing ones, mainly in data management areas.

### Linking & Import
- Improvements to linking from blendfiles (e.g. not systematically loading all data).
- [Embedded Linked Data](https://devtalk.blender.org/t/asset-embedding-proposal/34484) (hybrid between normal linking, and appending).
- Importing from other formats (the Collection Import idea). **[2024 Q4 ?]**
- The USD instancing project. **[2024 Q4]**

See also [this WIP ideas/designs](https://projects.blender.org/mont29/.profile/src/branch/main/paste_buffer/collection_linking_usd_assets_blendfile.md).

_NOTE: Some of these are technically owned by the Pipeline, Assets & I/O module, but will also require significant Core-related work._

### Overrides
- Investigate possibilities to improve reliability and resilience of liboverride.
- Add some missing liboverride features, like proper support for materials (although override of materials would not be useful currently, being able to [override materials assignement](https://projects.blender.org/blender/blender/issues/101552) would be).
- [Cherry-picking](https://projects.blender.org/blender/blender/issues/95816) (pre-requisite for e.g. usable liboverride of node trees, including shader nodes).
- [Dynamic overrides](https://projects.blender.org/blender/blender/issues/96144).

### Variants
[High-level design](https://devtalk.blender.org/t/2024-08-16-variants-design-workshop/36257) still has many interesting technical topics to be investigated.

### Partial Update of Blendfiles

The target of this work is to remove the current distinction between Asset and non-Asset blendfiles, which was introduced to allow the 'brush asset' project to edit and save brushes.

The technical design/ideas for this topic are fairly clear, but their practical consequences (and therefore the time required to fully implement it and address the after-math issues) are hard to predict. The 'corner-case' factor is likely to be high.
* Implement the "second stage" of the [Refactor of Partial Write and Beyond](https://projects.blender.org/blender/blender/issues/122061) design.
* Use these new 'i/o' features in the brush asset code area.
