# WIP!

## Collections as 'advanced linking/import' entry point

Add _one_ 'collection import' option per collection.

General idea is to bring closer Blender lib linking and importing data from other formats, and to allow more flexibility in what is loaded, allow reloading, etc. 

* Similar to 'collection export', can import from any non-native format which I/O code supports the feature (USD, FBX, glTF...).
* For formats that support it (blendfile liblinking, USD...), option to select a sub-set of (path into) the whole data.
* USD 'instancing' is data that is never loaded in Blender (only rendered).
* Actual import can happen after file opening...
* ... Or on-demand (controlled by the user, python scripts, etc.).
* User can re-import/update from file.
* Import done in background thread (and dedicated Main), only merging the temp Main into global one is blocking.
* Several types of import, from most shallow to complete?
  + Only root collection (_Is that one useful at all?_).
  + Collections/objects hierarchy, but not data (geometry, shading, animation).
    - And partial data import of chosen sub-hierarchies.
  + Full import.
* _Painpoint_:
  + How to allow other unrelated IDs in blendfile to reference data from imported collections?
  + How not to lose these reference on file save/load, when reloading external data, etc.?
  + Do we allow such reference at all?
* One of the supported 'import types' could be blenfiles' collections themselves.
  + Allows to manage blendfile linking in a similar way as 'linking' from external data.
  + Is there any actual interest/benefit to do this?
  + Some technical challenges here, since linked data are shared between all of their users.
* Could use 'embedded linking' system to optionnaly 'cache' imported data in the blendfile itself.
* Can create liboverrides to edit (a subset) of the imported data.

## Add flexibility to normal lib linking

Essentially bring the various 'loading' options described above for 'collection import' to regular lib linking too.

## UI/UX

* Collection properties for case-by-case management.
* Outliner for batch-management and overview
  + New view?
  + Most common options in contextual menu in common views too.
