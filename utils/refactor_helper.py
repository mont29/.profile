# SPDX-FileCopyrightText: 2025 Blender Authors
#
# SPDX-License-Identifier: GPL-2.0-or-later

import pathlib
import re
import sys


REGEX = re.compile(r"MEM_cnew_array<")


def process_file(file_path):
    with open(file_path, "r") as f:
        code = f.read()
    changed_code, num_changes = REGEX.subn(r"MEM_calloc_arrayN<", code)
    if num_changes:
        print(f"{file_path}: {num_changes} changes.")
        with open(file_path, "w+") as f:
            f.write(changed_code)
            


def process_source_code(root_path):
    for dirpath, dirnames, filenames in root_path.walk():
        for filename in filenames:
            if str(filename).startswith("."):
                continue
            process_file(dirpath / filename)


def main():
    import argparse

    parser = argparse.ArgumentParser(description="Refactor source code helper. Code must be edited before use!")
    parser.add_argument('-r', '--rootsource', default=".", help="Path to the root of the source code to refactor.")
    args = parser.parse_args(sys.argv[1:])

    process_source_code(root_path=pathlib.Path(args.rootsource))


if __name__ == "__main__":
    main()
