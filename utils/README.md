# Bash helpers

`.bashrc_blender` can be added to `.bashrc` file that way e.g. (together with a couple other 'useful' helpers):


```
# ----- Misc customization. ----- #

# GIT/Blender tooling.
if [ -f $HOME/.bashrc_blender ]; then
    source $HOME/.bashrc_blender
fi

# Reload this file into existing terminal.
alias BASHRC='source ~/.bashrc'

PATH="$PATH:~/.config/QtProject/qtcreator/externaltools"
PERFPROFILER_PARSER_FILEPATH="/usr/bin/perf"

# ----- END Misc customization. ----- #
```

# Refactor Helper Py Script

This is essentially a search-and-replace python script. Needs editing first to update the regex.
