# Weblate Slow Updates Investigations

## Server Config Tweaks
Temporary changes (overrides of Docker config), done by Oleg:
  * PostgreSQL cache increased to 2GB
  * Hard timeout for requests increased to 2h (?)

AFAIK these reset next time the docker image is updated (or even the server is restarted)?

## Weblate Findings
This was mainly found out by running own local instance of weblate, and reading its source code.

### Current Status
  * Languages updates are all done sequentially within a single process.
  * Language update takes 2-3 minutes per language when PO files change -> over 2h for our 52 languages.
  * Language updates after repo update is by default performed as part of the wsgi query. It only goes into a defferred Celery task if another update is already on-going!
  * Max timeout of a wsgi request seems to be 2h - which leaves several of the last to-be-processed languages not updated.
  * 'Rescan' command only update the languages if the underlying PO file is not matching - this fails to update failed languages from normal 'Update' command.
    * Will submit a PR upstream to enforce update from this command.
    * Work-around for now is to use the 'Reset' command, which does enforce rescan of PO files.

### Potential Improvements
  * Run update tasks in background systematically.
  * Update languages in parallel (launch several background tasks).