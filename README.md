# Bastien Montagne

## Senior Developer at Blender

Working mainly on low-level/support areas of the code (a.k.a. [Core module](https://projects.blender.org/blender/blender/wiki/Module:%20Core)), including runtime data-blocks management, file I/O, linking, appending, library overrides, undo system...

Also active in the [Pipeline, Assets & I/O](https://projects.blender.org/blender/blender/wiki/Module:%20Pipeline,%20Assets%20&%20I/O), [Platforms, Builds, Tests & Devices](https://projects.blender.org/blender/blender/wiki/Module:%20Platforms,%20Builds,%20Tests%20&%20Devices), and [Manual & UI Translations](https://translate.blender.org/) modules.

### Contacts
* [Chat](https://matrix.to/#/@mont29:blender.org)
* [Devtalk](https://devtalk.blender.org/u/mont29/)

